﻿1
00:00:03,593 --> 00:00:04,903
Selam. Benim adım Evan.

2
00:00:04,903 --> 00:00:07,279
Facebook'ta Çözüm Mühendisiyim.

3
00:00:07,279 --> 00:00:11,652
Bugün size Facebook Potansiyel Müşteri Reklamlarını kullanarak gerçek zamanlı güncellemeler

4
00:00:11,652 --> 00:00:14,157
için tam entegrasyonun nasıl yapılacağını göstereceğim.

5
00:00:14,157 --> 00:00:15,965
Bugün iki dosya oluşturacağız.

6
00:00:15,965 --> 00:00:20,131
Bir tanesi pazarlama veya CRM platformunuzu taklit edecek.

7
00:00:20,131 --> 00:00:24,118
İkinci dosya ise gerçek zamanlı güncellemeleri alan webhook olacak.

8
00:00:25,245 --> 00:00:28,216
Bugünkü demo için üç şeye ihtiyacımız var.

9
00:00:28,216 --> 00:00:30,034
İlki bir Facebook Uygulaması.

10
00:00:30,034 --> 00:00:33,028
İkincisi Facebook JavaScript SDK.

11
00:00:33,028 --> 00:00:37,783
Üçüncüsü ise internetten erişilebilen bir web sunucusu.

12
00:00:38,869 --> 00:00:41,452
Facebook uygulamamı buraya kurmuştum.

13
00:00:41,452 --> 00:00:45,517
Siz de developers.facebook.com adresinde bunu kendiniz yapabilirsiniz.

14
00:00:45,517 --> 00:00:47,095
Kurulum oldukça basit.

15
00:00:47,095 --> 00:00:52,335
Burada yaptığım tek şey Uygulama Domainlerini eklemek.

16
00:00:52,335 --> 00:00:56,373
Bu Uygulamada Domainleri kullandığım Heroku URL'sini ve

17
00:00:56,373 --> 00:00:58,067
yerel ana bilgisayarları içeriyor.

18
00:00:58,067 --> 00:01:02,751
Benim tarafımdaki test işlemlerini biraz hızlandırmak için ana bilgisayarları ekledim.

19
00:01:02,751 --> 00:01:05,765
Buraya birkaç tane domain eklemek istiyorsanız bu alan için URL'leri beyaz listeye

20
00:01:05,765 --> 00:01:09,902
almak üzere birkaç platform eklemeniz gerektiğini unutmayın.

21
00:01:12,453 --> 00:01:16,890
Şimdi ilk yapmak istediğim şey Facebook'a uygulamamın

22
00:01:16,890 --> 00:01:20,273
"leadgen" olayıyla ilgilendiğini söylemek.

23
00:01:20,273 --> 00:01:23,116
Bunu burada web ana bilgisayarı sekmesinde yapabilir

24
00:01:23,116 --> 00:01:27,323
ve yeni bir abonelik ekleyebilirim.

25
00:01:27,323 --> 00:01:30,021
Ancak ben bunu Graph Explorer veya API'ler üzerinden yapacağım.

26
00:01:30,021 --> 00:01:32,972
Böylece API çağrısının nasıl bir şey olduğunu görebiliriz.

27
00:01:34,674 --> 00:01:39,135
Sağ üst köşede uygulamanın az önce oluşturduğum uygulama

28
00:01:39,135 --> 00:01:41,985
olarak ayarlandığından emin olacağım.

29
00:01:41,985 --> 00:01:44,485
Bir Uygulama Jetonu alacağım.

30
00:01:44,485 --> 00:01:48,055
API çağrısı, Uygulama Kodu olacak.

31
00:01:48,055 --> 00:01:50,773
Kodu uygulama panosundan alabiliriz.

32
00:01:55,384 --> 00:01:56,977
Ve "subscriptions" ucu.

33
00:01:58,363 --> 00:02:01,404
Bu bir "post" isteği ve dört alana ihtiyacımız var.

34
00:02:04,856 --> 00:02:06,410
"Object" öğesi "page" olacak.

35
00:02:06,410 --> 00:02:08,887
Virgülle ayrılmış değer kabul eden "fields" öğesi

36
00:02:08,887 --> 00:02:10,234
"leadgen" olacak

37
00:02:10,234 --> 00:02:14,162
"verify_token" öğesi "abc123" olacak.

38
00:02:14,162 --> 00:02:16,383
İsterseniz daha güçlü bir tane seçebilirsiniz.

39
00:02:16,383 --> 00:02:20,976
"callback_url" ise Heroku sunucum olacak.

40
00:02:20,976 --> 00:02:22,827
Daha hiçbir şeyi ayarlamadım.

41
00:02:22,827 --> 00:02:27,582
Sunucuya gidelim ve webhook.php dosyasını ekleyelim.

42
00:02:27,582 --> 00:02:29,537
Şimdi isteğimizi gönderip neler olacağını görelim.

43
00:02:30,643 --> 00:02:33,303
Çıkan bildirim doğrulamanın başarısız olduğunu söylüyor.

44
00:02:33,303 --> 00:02:36,116
Bunun nedeni webhook.php dosyasının var olmaması.

45
00:02:36,116 --> 00:02:40,654
Olsaydı da, gerçek zamanlı güncelleme kurulumu için

46
00:02:40,654 --> 00:02:44,114
doğru şekilde yapılandırılmadığından çalışmayacaktı.

47
00:02:44,114 --> 00:02:46,623
Şimdi bunu hızlıca yapacağız.

48
00:02:46,623 --> 00:02:48,900
Şu dizine gideceğiz.

49
00:02:48,900 --> 00:02:50,628
Dizin boş, içinde hiçbir şey yok.

50
00:02:50,628 --> 00:02:54,695
Heroku'nun bu php'yi tanıması için bir composer.json dosyası.

51
00:02:54,695 --> 00:02:56,798
webhook.php dosyasını oluşturacağız.

52
00:02:58,382 --> 00:03:01,882
Gerçek zamanlı güncelleme aboneliği şöyle çalışıyor.

53
00:03:01,882 --> 00:03:05,945
İki parametreyle bir "get" isteği yapacağız:

54
00:03:05,945 --> 00:03:08,109
"hub_challenge" ve "hub_verify_token".

55
00:03:08,109 --> 00:03:10,556
Şimdi komut dosyasının bunları bildiğinden emin olalım.

56
00:03:22,799 --> 00:03:26,270
Harika. Şimdi doğrulama jetonunun, API çağrısını yaparken

57
00:03:26,270 --> 00:03:29,321
ayarladığımız jeton olduğundan emin olmamız gerek.

58
00:03:29,321 --> 00:03:33,501
Doğru jetonsa, sınamayı ekranda göstereceğiz.

59
00:03:33,501 --> 00:03:37,483
Facebook "get" talebi içinde gönderen sınamayı görürse,

60
00:03:37,483 --> 00:03:42,083
bu uç noktanın iyi olduğuna karar verir ve abonelik başarılı olur.

61
00:03:42,083 --> 00:03:42,989
Evet başarılı.

62
00:03:53,861 --> 00:03:55,986
Harika. Kodu hızlıca kontrol edelim.

63
00:03:57,074 --> 00:03:57,959
İyi görünüyor.

64
00:04:00,475 --> 00:04:04,925
Şimdi bunu Heroku sunucuma göndereceğim.

65
00:04:12,312 --> 00:04:13,821
Biraz bekleyelim.

66
00:04:30,469 --> 00:04:32,614
Harika. Artık yayınlandığına göre

67
00:04:32,614 --> 00:04:34,953
API çağrısını yeniden deneyebiliriz.

68
00:04:37,996 --> 00:04:39,652
"success" için "true" sonucunu alıyoruz.

69
00:04:39,652 --> 00:04:42,357
Webhook ayarlandı.

70
00:04:42,357 --> 00:04:44,407
Şimdi ugulama panosuna geri dönüp

71
00:04:44,407 --> 00:04:46,762
Webhooks sekmesinde olup olmadığını görelim.

72
00:04:48,604 --> 00:04:50,962
İşte burada. Harika.

73
00:04:52,771 --> 00:04:57,812
Açık nedenlerden dolayı, Facebook'ta her form doldurulduğunda

74
00:04:57,812 --> 00:05:01,281
uygulamanıza bir yük göndermeyeceğiz.

75
00:05:01,281 --> 00:05:05,253
Bunu uygulamaya izin veren sayfalarla

76
00:05:05,253 --> 00:05:06,985
sınırlandırmamız gerekiyor.

77
00:05:06,985 --> 00:05:11,917
Bir reklamveren platformunuza giriş yaptığında ise şöyle görünecek.

78
00:05:11,917 --> 00:05:15,556
Reklamveren, Facebook Bağlan veya Facebook Girişi ile

79
00:05:15,556 --> 00:05:17,936
uygulamanıza bağlanacak.

80
00:05:17,936 --> 00:05:22,115
Siz sayfaları yönetme izni talep edeceksiniz.

81
00:05:22,115 --> 00:05:26,191
Reklamveren onaylarsa, sayfalarını uygulamanıza

82
00:05:26,191 --> 00:05:28,748
abone yapmak için onların erişim jetonunu kullanabilirsiniz.

83
00:05:28,748 --> 00:05:33,222
Şimdi, bunun platform.php adlı yeni bir dosyada

84
00:05:33,222 --> 00:05:36,435
nasıl göründüğüne hızlıca bakacağım.

85
00:05:36,435 --> 00:05:41,061
Bu platform.php dosyası Pazarlama veya

86
00:05:41,061 --> 00:05:44,103
CRM platformunuzu temsil edecek.

87
00:05:47,848 --> 00:05:51,984
Bilgisayarımda yerel bir web sunucusu olacak.

88
00:06:05,664 --> 00:06:06,624
Harika.

89
00:06:06,624 --> 00:06:10,057
İlk ihtiyacımız olan şey Facebook Javascript SDK.

90
00:06:10,057 --> 00:06:13,087
Bunu developers.facebook.com adresinde bulabilirsiniz.

91
00:06:13,087 --> 00:06:16,316
Bu kısmı kopyalayıp yapıştırabiliriz.

92
00:06:18,877 --> 00:06:24,261
Uygulama Kodunu kendi Uygulama Kodumuzla değiştirmemiz gerektiğini unutmayalım.

93
00:06:24,261 --> 00:06:26,378
Kodumuzu Pano'da bulabiliriz.

94
00:06:32,159 --> 00:06:37,303
Ayrıca ekranda bir Facebook giriş düğmesi göstermek istiyoruz.

95
00:06:37,303 --> 00:06:41,302
Bunun için ekranda aşağı inip bunu alacağım.

96
00:06:42,624 --> 00:06:47,357
Bu şekilde html'de bir Facebook giriş düğmesi oluşacak

97
00:06:47,357 --> 00:06:51,156
ve Facebook Girişi işlevini çalıştıracak.

98
00:06:57,889 --> 00:06:59,257
Nasıl göründüğüne bir bakalım.

99
00:07:01,568 --> 00:07:05,510
Bu uygulamada daha önce kimlik doğrulaması yapmadığım için

100
00:07:05,510 --> 00:07:10,857
giriş iletişim kutusunu açacak ve

101
00:07:10,857 --> 00:07:12,057
sayfalarımı yönetmek için izin isteyecek.

102
00:07:13,853 --> 00:07:17,263
Benden istediği şey "publish_actions".

103
00:07:17,263 --> 00:07:19,213
Ama ben bunu istemiyorum.

104
00:07:19,213 --> 00:07:23,581
Uygulamamın istediği izinler bu.

105
00:07:23,581 --> 00:07:26,233
Bunu "manage_pages" olarak değiştirelim.

106
00:07:26,233 --> 00:07:32,924
Bazı bilgileri ekrana getirmek için bu geri aramayı değiştirelim.

107
00:07:43,197 --> 00:07:44,344
Harika.

108
00:07:44,344 --> 00:07:45,648
Hadi sayfayı yenileyelim.

109
00:07:45,648 --> 00:07:47,039
Konsolu açın.

110
00:07:51,902 --> 00:07:55,884
Lead Gen Integration Test sayfalarınızı yönetmek istiyor.

111
00:07:55,884 --> 00:07:57,192
Tamam.

112
00:07:57,192 --> 00:07:59,044
Başarılı.

113
00:07:59,044 --> 00:08:03,300
Bu yanıt objesi, bağlantı kurduğumuzu söylüyor.

114
00:08:05,002 --> 00:08:10,188
Uygulamayla başarıyla bağlantı kurduğumuza göre

115
00:08:10,188 --> 00:08:12,809
şimdi uygulamanın yapmak isteyeceği şey

116
00:08:12,809 --> 00:08:16,703
kullanıcıların erişimi olan sayfaların listesini verip hangileri için

117
00:08:16,703 --> 00:08:22,415
gerçek zamanlı güncellemeler almak istediklerini sormak olacak.

118
00:08:22,415 --> 00:08:27,238
Bunu da sayfaların listesini almak için bir API çağrısı yaparak gerçekleştireceğiz.

119
00:08:27,238 --> 00:08:32,340
fb.api ve /me/

120
00:08:32,340 --> 00:08:34,139
accounts diyoruz.

121
00:08:43,929 --> 00:08:47,566
Devam edip "console.log" yapabiliriz.

122
00:08:57,284 --> 00:08:59,825
Şimdi sayfayı yenileyelim ve nasıl göründüğüne bakalım.

123
00:09:01,813 --> 00:09:05,265
Platformda daha önce kimlik doğrulaması yaptığım için

124
00:09:05,265 --> 00:09:07,108
tekrar yapmamı istemeyecek.

125
00:09:07,108 --> 00:09:09,462
Sayfaları aldım.

126
00:09:09,462 --> 00:09:15,180
Bu yanıt objesinde bir veri objesi olduğunu göreceksiniz,

127
00:09:17,277 --> 00:09:21,212
erişimim olan sayfaların bir listesini içerecek.

128
00:09:21,212 --> 00:09:28,958
Puppy sayfasını kullanacağız.

129
00:09:28,958 --> 00:09:34,944
Bu sayfanın kendi erişim jetonu olduğuna dikkat edin.

130
00:09:34,944 --> 00:09:37,784
Bu, sayfamı uygulamaya abone yapmak için

131
00:09:37,784 --> 00:09:41,397
ihtiyacım olan sayfa erişim jetonu.

132
00:09:43,536 --> 00:09:45,216
Şimdi sayfalarımın bir listesi olduğuna göre

133
00:09:45,216 --> 00:09:48,342
bu listeyi ekranda göstermek istiyorum,

134
00:09:48,342 --> 00:09:52,071
böylece kullanıcının konsola gidip kodları çekmesi gerekmeyecek.

135
00:09:52,071 --> 00:09:57,201
Şimdi sayfaları ekleyebileceğimiz

136
00:09:57,201 --> 00:10:01,856
bir "ul" öğesi oluşturalım.

137
00:10:04,903 --> 00:10:06,511
Bir "for" döngüsü oluşturacağım.

138
00:10:10,986 --> 00:10:15,381
"pages" öğesinin "data" objesi olduğunu hatırlıyoruz.

139
00:10:24,248 --> 00:10:28,423
Ayrıca "ul" öğesini de kullanabilmek istiyoruz.

140
00:10:39,471 --> 00:10:44,191
Peki şimdilik sayfa adlarını "ul" objesine ekleyelim.

141
00:11:00,633 --> 00:11:02,474
Sayfayı yenileyelim.

142
00:11:05,143 --> 00:11:06,213
Facebook'a giriş yapıyorum.

143
00:11:06,213 --> 00:11:07,545
Evet.

144
00:11:07,545 --> 00:11:11,490
Kullanıcıların erişimi olan sayfaların listesi karşımızda.

145
00:11:11,490 --> 00:11:14,733
Bu çok basit bir kullanıcı arayüzü.

146
00:11:14,733 --> 00:11:19,115
Şimdi bunları bağlantılara dönüştürelim, böylece kullanıcı

147
00:11:19,115 --> 00:11:23,660
tıklayıp bu sayfalar için gerçek zamanlı güncellemeler isteyebilir.

148
00:11:25,657 --> 00:11:32,642
Sadece bir liste maddesi yerine, bir bağlantı öğesi göstereceğiz.

149
00:11:37,519 --> 00:11:41,142
"innerHTML" bir bağlantı öğesi olacak.

150
00:11:50,473 --> 00:11:57,956
Bağlantı öğesi için "onclick" ise

151
00:11:57,956 --> 00:12:00,547
uygulayacağım bir işleve gidecek.

152
00:12:00,547 --> 00:12:02,628
Şimdilik sadece "subscribeApp" diyelim.

153
00:12:06,845 --> 00:12:08,684
Erişim jetonunu da vereceğimizi unutmayalım

154
00:12:08,684 --> 00:12:11,629
çünkü API çağrısını yaparken ihtiyacımız olacak.

155
00:12:11,629 --> 00:12:16,567
Bu "a" öğesi liste öğesine girecek.

156
00:12:21,169 --> 00:12:22,838
Harika. Şimdi nasıl göründüğüne bakalım.

157
00:12:22,838 --> 00:12:26,434
Önce "subscribe" yapalım.

158
00:12:34,512 --> 00:12:35,965
"console.log" yapalım.

159
00:12:48,518 --> 00:12:49,684
Sayfayı yenileyelim.

160
00:12:51,003 --> 00:12:52,074
Facebook'la giriş yapalım.

161
00:12:52,074 --> 00:12:53,625
Şimdi bağlantılarımız var.

162
00:12:53,625 --> 00:12:56,291
Puppy sayfasını abone yapmak istediğimi varsayalım.

163
00:12:56,291 --> 00:12:59,653
Tıklıyorum ve başarılı sonuç veriyor.

164
00:13:01,650 --> 00:13:04,501
Ancak aslında sayfayı uygulamaya abone yapmadık,

165
00:13:04,501 --> 00:13:07,341
bunu başka bir API çağrısı kullanarak yapacağız.

166
00:13:07,341 --> 00:13:11,372
Bu öğe sayfa düğümü olacak,

167
00:13:12,372 --> 00:13:14,059
yani sadece sayfa kodu ve de

168
00:13:14,059 --> 00:13:20,111
o sayfanın "subscribed_apps" ucunda.

169
00:13:29,392 --> 00:13:30,297
Evet...

170
00:13:37,514 --> 00:13:40,277
...yanıt için "console.log" yapalım.

171
00:13:40,277 --> 00:13:43,364
Yanıt muhtemelen "success" için "true" değeri verecek

172
00:13:43,364 --> 00:13:47,450
ama yine de kaydını almak için çalıştıralım.

173
00:13:48,513 --> 00:13:50,734
Sayfayı yenileyelim.

174
00:13:50,734 --> 00:13:52,371
Bir kez daha giriş yapalım.

175
00:13:53,671 --> 00:13:56,826
Puppy sayfasını abone yapacağız.

176
00:13:58,200 --> 00:14:01,692
Başarıyla abone olduk ve bir hata aldık.

177
00:14:01,692 --> 00:14:05,007
Hata aldık çünkü erişim jetonunu kullanmayı unuttuk ve

178
00:14:05,007 --> 00:14:10,031
bunu bir "post" isteği yapmayı da unuttuk.

179
00:14:10,031 --> 00:14:12,224
Şimdi bunu hızlıca düzeltelim.

180
00:14:13,900 --> 00:14:19,218
Bu fonksiyondaki ikinci parametre "post" olabilir.

181
00:14:19,218 --> 00:14:23,925
Üçüncü parametre de erişim jetonu olacak.

182
00:14:31,598 --> 00:14:32,522
Tekrar deneyelim.

183
00:14:39,044 --> 00:14:41,302
Harika. Şimdi "success: true" sonucu aldık.

184
00:14:41,302 --> 00:14:45,765
Aboneliğin gerçekleştiğini doğrulamak istersek

185
00:14:45,765 --> 00:14:47,512
Graph Explorer'a dönmemiz gerekir.

186
00:14:55,179 --> 00:14:58,872
Yine aynı "subscribed_apps" ucunu gireceğiz

187
00:14:58,872 --> 00:15:02,562
ama bu kez sayfa jetonunu kullanarak "get" isteği yapacağız.

188
00:15:05,095 --> 00:15:08,039
Şimdi Lead Gen Integration Test'in bu sayfaya

189
00:15:08,039 --> 00:15:10,420
abone olduğunu görebiliriz.

190
00:15:11,778 --> 00:15:16,439
Şimdi yapacağımız şey, uç noktamızın

191
00:15:16,439 --> 00:15:20,462
Facebook'tan başarıyla ping aldığını test etmek.

192
00:15:20,462 --> 00:15:23,062
Bunu da mevcut webhook.php dosyasını

193
00:15:23,062 --> 00:15:25,908
değiştirerek yapacağız çünkü

194
00:15:25,908 --> 00:15:30,004
burası gerçek zamanlı güncellemeleri alacağımız aynı uç nokta.

195
00:15:32,111 --> 00:15:37,455
Güncellemeler "post body" içinde alınacak.

196
00:15:37,455 --> 00:15:40,005
Yani verileri "post body"

197
00:15:41,809 --> 00:15:44,888
içinden alabilirsiniz.

198
00:15:44,888 --> 00:15:47,649
Şimdilik sadece "error_log" yapacağım.

199
00:15:47,649 --> 00:15:49,626
Siz verileri istediğiniz gibi kullanabilirsiniz.

200
00:15:49,626 --> 00:15:54,177
Ben "error_log" yapacağım için, kayıtları Heroku'da ekleyebilirim

201
00:15:54,177 --> 00:15:57,274
ve verileri geldikleri anda görebiliriz.

202
00:16:14,422 --> 00:16:16,871
Harika.

203
00:16:16,871 --> 00:16:19,131
Şimdi bunu Heroku'ya gönderelim.

204
00:16:33,448 --> 00:16:36,552
O devam ederken biz de Power Editor'da

205
00:16:38,428 --> 00:16:40,255
bir Potansiyel Müşteri reklamı oluşturabiliriz.

206
00:16:42,441 --> 00:16:46,586
Ben önceden yapmıştım.

207
00:16:47,983 --> 00:16:51,113
Mobilde Gör düğmesine tıklayarak

208
00:16:51,113 --> 00:16:54,507
entegrasyonu test edebilirsiniz.

209
00:16:54,507 --> 00:16:57,156
Yüklendikten sonra göstereceğim.

210
00:16:57,156 --> 00:17:01,557
Reklama tıklayalım ve sağdaki Düzenle sekmesine gidelim.

211
00:17:01,557 --> 00:17:02,748
Sekmeyi açalım.

212
00:17:03,986 --> 00:17:08,435
Mobilde Gör bağlantısı formu doldurmanız için

213
00:17:08,435 --> 00:17:10,596
telefonunuza bir anında ilet bildirimi gönderecek.

214
00:17:10,596 --> 00:17:12,390
Küçük bir ipucu...

215
00:17:12,390 --> 00:17:16,963
Bu bağlantı çalışmazsa, ekranınızın kapalı olduğundan emin olun.

216
00:17:16,963 --> 00:17:20,235
Bildirim bazen bildirimler sekmesinde görünmeyebiliyor.

217
00:17:23,418 --> 00:17:26,631
Buraya tıkladığımda telefonuma bir bildirim

218
00:17:26,631 --> 00:17:29,381
gönderildiğini söyleyecek.

219
00:17:29,381 --> 00:17:32,044
Hazır olur olmaz da formu dolduracağım.

220
00:17:32,044 --> 00:17:33,619
Tamam. Hazır gibi görünüyor.

221
00:17:33,619 --> 00:17:36,842
Heroku kayıtlarımı alacağım.

222
00:17:41,400 --> 00:17:42,319
Biraz yer bırakalım.

223
00:17:42,319 --> 00:17:44,176
Şimdi formu dolduracağım.

224
00:17:58,232 --> 00:17:59,976
Birkaç saniye bekleyelim.

225
00:18:07,344 --> 00:18:10,732
Gerçek zamanlı güncelleme geldi. Harika.

226
00:18:10,732 --> 00:18:15,779
Bu veriyi alıp istediğiniz gibi kullanabilirsiniz.

227
00:18:15,779 --> 00:18:20,712
"leadgen_id" içinde ihtiyacınız olan tüm veriler var

228
00:18:20,712 --> 00:18:25,310
veya potansiyel müşteri formuyla gelen tüm veriler.

229
00:18:25,310 --> 00:18:29,280
Verilerin içinde neler olduğunu görmek için

230
00:18:30,874 --> 00:18:32,975
bu koda bir "get" isteği yapabilirsiniz.

231
00:18:32,975 --> 00:18:34,070
Hepsi bu kadar.

232
00:18:35,130 --> 00:18:37,200
Demomuz sona erdi.

233
00:18:37,200 --> 00:18:42,994
Umarım tam bir uçtan uca entegrasyon yapmanıza yardımcı olur.

234
00:18:42,994 --> 00:18:43,922
İzlediğiniz için teşekkürler.

